# Notepad

![Notepad](docs/.vuepress/public/img/logo.webp)

## How To Build

Built with Vuepress

### Clone

```shell
npm install
```

### Local Dev

```shell
npm run serve
```

## How To Deploy

Deployments handled via CI, see `.gitlab-ci.yml` for script details
