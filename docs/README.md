---
title: Home
home: true
heroImage: "img/logo.webp"
heroText:
tagline: 🦄 + 🌈 + 🤦‍♀️
actions:
  - text: Latest 	→
    link: /kubernetes/
    type: primary
features:
  - title: Built with VuePress
    details: Minimal setup with markdown-centered project structure.
  - title: Stored In GitLab
    details: Repository stored in GitLab, built with GitLab Pipelines.
  - title: Hosted In GitLab Pages
    details: GitLab Pipelines deploys the VuePress build to GitLab Pages.
footer: © 2021-present 😸
---
