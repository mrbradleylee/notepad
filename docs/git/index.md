---
title: Git Cheat Sheet
---

Quick Git cheat sheet, cause, we always seem to forget...
Tailored towards my personal GitLab flow.

## External Reference

[GitLab Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

## Aliases

I use Ohmyzsh shell, which includes some aliases OOTB that I use.

- `ggl|p` = `git pull|push origin $(current_branch)`
- `gco` = `git checkout`
  - `gco -b 'branchname'`
- `gaa` = `git add --all`
- `gcam` = `git commit -a -m`
  - `gcam 'commit message'`
- `ggpush|pull` = `git push|pull origin '$(git_current_branch)'`

## Clone

```shell
git clone <reponame>
```

::: tip
If you have multiple SSH keys set up, you can use the hosts file to use
different keys for different accounts and repos in the same system. These
generally require modifying the clone URL to match the host in your local
machine.
:::

Add `-v` for forked projects

## Remote

If you cloned from a remote, it should be set. If you `git init` from a local
repo, you will need to set your remote origin.

### View

```shell
git remote -v
```

### Add

```shell
git remote add <name> <git@host:repo.git>
```

For example, this repo, with a remote named `origin`:

```shell
git remote add origin git@gitlab.com:mrbradleylee/notepad.git
```

## Ignore

Basic ignore strings for my projects:

```gitignore
# Node deps
node_modules

# OSX
.DS_Store

# output dirs
dist
public
```

## Status

```shell
git status
```

Show current status against the remote

## Branch

GitLab workflow creates MRs for a push to a remote branch. Use alias to create
new branch:

### List

```shell
git branch
```

### Create New

```shell
git checkout -b 'branch-name'
```

The `-b` will create the branch if it doesn't exist. To switch, drop it.

### Delete

```shell
git branch -d 'branchname'
```

#### Delete All Remote Branches != `main`

```shell
git branch -r | grep 'origin' | grep -v 'main$' | grep -v HEAD | cut -d/ -f2- | while read line; do git push origin :heads/$line; done;
```

## Stage

### Stage - Add Files

```shell
git add <filename>
```

:::tip
use alias `gaa` or `git add --all` to stage all your local changes
:::

### Unstage - Remove Files

Unstage can be use to remove files that are staged for a commit:

```shell
git rm <filename>
```

:::warning
Unstage does NOT remove files from the project, just from being made git aware
:::

## Commit

Commits act as local versioning. Committed changes will be pushed to remote
when requested.

```shell
git commit -m 'commit message'
```

Alternatively, you can use the `gcam` or full command to also add all modified
and deleted files, but not files that haven't been "added."

```shell
git commit -a -m 'commit message'
```

### Amend

You should create another commit if it's already been pushed to the remote,
however, if you need to amend a commit that's local, you can do so with:

```shell
git commit --amend
```

## Push

Push changes to your remote repository. In GitLab, this activity generally
triggers a pipeline run if a `.gitlab-ci.yml` exists or AutoDevOps is enabled.
For the GitLab flow, you can specify the current branch, which will provide
a link to create a new merge request.

```shell
git push origin '<branchname>'
```

:::danger
It is generally not recommended to push to a `main` or `master` branch.
:::

## Merge

Two options here, `merge` or `rebase`. Both solve the same fundamental problem
of integrating a change from one branch into another branch.

[Reference](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

### Merging

Check out your feature branch with `git checkout <branch>`, then merge to a
target branch (`main` in this example) with:

```shell
git merge main
```

Merging maintains your traditional project history path, where the feature
branch is visible in the history merging back to the main branch:

```shell
     feature
    ________
   /        \
----------------- main

```

### Rebase

Rebase simply moves the feature branch modifications into the target:

From your feature branch:

```shell
git rebase main
```

:::danger
Rebasing can be destructive to public branches
:::

A rebased history will look like:

```shell
      feature
     --------
    /        \
----          ------ main

```

## Reset

If you're looking for resetting a local work area from a branch

```shell
git reset --hard origin/main
```

This resets the work area from `origin/main` and the `--hard` flag
discards any changes and leaves modifications as unstaged. You can use
`git status` to see the diffs.
