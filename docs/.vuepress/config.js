import { defineUserConfig } from "@vuepress/cli";
import { defaultTheme } from "@vuepress/theme-default";

export default defineUserConfig({
  lang: "en-US",
  title: "Notepad",
  description: "Muh Notes",
  head: [["link", { rel: "icon", href: "/notepad/favicon/favicon.ico?v=2" }]],
  base: "/notepad/",

  // configure theme
  theme: defaultTheme({
    logo: "img/logo.webp",
    repo: "https://gitlab.com/mrbradleylee/notepad",
    docsDir: "docs",
    navbar: [
      { text: "Home", link: "/" },
      { text: "Git", link: "/git" },
      {
        text: "Kubernetes",
        children: [
          "/kubernetes/",
          "/kubernetes/docker-basics",
          "/kubernetes/kubernetes-basics",
          "/kubernetes/managing-applications",
          "/kubernetes/exposing-applications",
          "/kubernetes/running-applications",
          "/kubernetes/storage",
        ],
      },
    ],
    // colorModeSwitch: false,
    sidebarDepth: 3,
    sidebar: {
      "/kubernetes/": [
        "/kubernetes/",
        "/kubernetes/docker-basics",
        "/kubernetes/kubernetes-basics",
        "/kubernetes/managing-applications",
        "/kubernetes/exposing-applications",
        "/kubernetes/running-applications",
        "/kubernetes/storage",
      ],
      "/test/": ["/test/", "/test/first", "/test/second"],
      "/": [""],
    },
  }),

  // configure markdown
  markdown: {
    toc: {
      level: [2, 3, 4],
    },
    headers: {
      level: [2, 3, 4],
    },
  },
});
