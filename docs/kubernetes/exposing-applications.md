---
title: Exposing Applications
---

## Exposing Applications in K8s

In this section we'll cover the basics of Kubernetes networking to make
usable applications.

### Understanding K8s Networks

If you extend the diagram below, you can see the basic moving parts of an
application deployment and it's connectivity.

Each `pod` will have an internal IP, which is controlled by the `deployment`,
which uses a `service` to control network access to the application.

For `http` and `https` traffic, we can use an `ingress` to navigate through
a service to the deployments for those protocols ONLY.

::: details Basic Kubernetes Network Diagram
![Kubernetes Network](/img/docs/kubernetes/k8s-network.webp)
:::

### Networking Within A Pod

A `pod` runs containers, and it contains the IP address for itself.
Containers that are running within the pod will share the same IP as the pod
itself as the pod is managed and the containers inside are not.

The analogy here is a smartphone: Each app is a container, and doesn't need
it's own address since the phone is managing the apps.

So technically, "Networking within a pod" doesn't really exist, since the pod
is the endpoint itself. 🤯

### Managing Services

Again we'll use the diagram as a reference. Note the blue pods are
representative of physical worker nodes in a cluster.

Service objects connect deployments using labels. The service object needs to
connect to the pods using several arguments, including:

```yaml
ipAddress:
targetPort:
endpoints:
```

The service communicates with the pods via `kube-proxy`, which itself uses
`iptables` to generate forwarding rules to the pods and containers within the
physical node.

::: details Basic Kubernetes Service Diagram
![Kubernetes Network](/img/docs/kubernetes/k8s-network.webp)
:::

#### Service Types

##### ClusterIP

Default type, internally accessible only

##### NodePort

Allocates a specific port that needs to be opened

##### LoadBalancer

Only in public cloud, gives an external IP address

##### ExternalName

Uses DNS to redirect

##### Empty

No selector specified, for direct connections based on IP/Port, without
and endpoint. Useful for database connections, or between namespaces.

#### expose

As a basic example, let's take a look at how to create a service using
`kubectl expose` to send traffic on external port 8000 to an nginx deployment
named `nginx`, using default internal port 80:

```shell
kubectl expose deployment nginx --port=80 --target-port=8000
```

### DNS in Kubernetes

`coredns` is the default Kubernetes DNS service. It resides in the `kube-system`
namespace.

DNS entries are created automatically on pod creation.

### Using Ingress

Ingress exposes HTTP and HTTPS routes to services running in a container.
It can be used for multiple purposes, such as:

- Externally reachable URL
- Load balancing
- TLS/SSL termination

Ingress requires an Ingress Controller to work, and again ONLY exposes
HTTP and HTTPS. Setting up an Ingress Controller will be different depending
on what your running your clusters on.
