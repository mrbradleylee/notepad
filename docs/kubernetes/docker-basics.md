---
title: Docker Basics
---

## Docker Basics

### Docker Architecture

Docker is comprised of these major pieces:

#### Image

An `image` is a static snapshot of an application and it's dependencies.

#### Container

A `container` is an isolated space for an `image` to run.
A running `container` requires it to be based off of an `image`, though
actions performed within the container do not affect the base `image` unless
the changes are committed into a new `image`.

::: tip
_Containers are applications_. They are ephemeral, and if not given a task,
will shut themselves down.
:::

### Creating Images

You can create an image in a few ways

#### From a running container

`docker commit <container>` and then `docker tag <image>` to create then tag an image.

#### Through a Dockerfile

A file that contains all the instructions to build a Docker image,
which allows you to automate container builds

Benefits include being able to distribute a Dockerfile instead of an
entire image.

Use `docker build .` to build the image in the current directory.

##### Arguments

**`FROM`**: What to base the image on, eg:
`ubuntu`, `alpine`, `node:latest`, etc.

```dockerfile
FROM ubuntu:latest
```

**`ADD`**: If you want to add anything internally. eg, adding external repo
to the container :w
repo.

```dockerfile
ADD test.repo /etc/repo
```

**`RUN`**: Things you want to install before running your main CMD.

```dockerfile
RUN npm -install
```

**`CMD`**: What the container executes, eg `npm run serve`.
If you do not provide a CMD, the container will loop with nothing to do.
This was the error in our node-11ty AutoDevOps pipeline.
Spaces in the CMD need to be `,` separated.

```dockerfile
CMD [ "npm", "run", "serve" ]
```

Full example:

```dockerfile
FROM node:lts-alpine

# install simple http server for serving static content
RUN npm install -g http-server

# make the 'app' folder the current working directory
WORKDIR /src

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build

# open port
EXPOSE 5000

# execute http-server _public --port 5000
CMD [ "http-server", "_public", "--port", "5000"]
```

### Container Operations

Actions that can be executed against containers, such as running, stopping,
cleaning, etc.

#### docker run

Containers can be thought of as applications, which will execute the CMD
it was designated to do, then stop.

Let's take a look at these three examples and intended behavior:

```shell
docker run ubuntu:latest
```

This will run ubuntu:latest image as a container, which executes a `/bin/bash`
shell. Since we don't provide it any values, it will close once it finishes
launching the shell.

```shell
docker run -it ubuntu:latest
```

The `-it` argument tells us to run an _interactive terminal_.
We'll be presented with the `/bin/bash` prompt inside the running container
with which we can execute commands inside the container.

You could use a `cmd+P` `cmd+Q` to exit the container and leave it running.

```shell
docker run --rm -it ubuntu:latest
```

The `--rm` flag added here tells docker to remove the container once the
instructions are complete.

#### docker rm[i]

`docker rm` removes a container.

`docker rmi` deletes an image from the local drive. This will fail if the
image is in use in a container, running or not.

`docker rmi --force` force deletes an image and all associated containers.

### Docker Networking

#### Diagram

`172.17.0.0/16`

![Docker Network Diagram](/img/docs/kubernetes/docker-network.webp)

#### Port Mapping

Like the `EXPOSE` flag in the `dockerfile`, we can map a port in the run
command as well using the `-p` argument:

```shell
docker run -d -p 8080:80 --name="apache" -v /var/www/html:/www/html httpd
```

This command launches a container that:

- is daemonized via `-d` so it'll run in the background
- maps port 8080 on the host to port 80 within the container via `-p`
- names the container `apache` via `--name`
- mounts `/var/www/html` on the host to `/www/html` within the container via `-v`
  - see [Docker Storage](#docker-storage) below for more info on bind mounts
- uses the `httpd` image

### Docker Storage

The `-v` flag allows you to define a volume or bind mount to mount paths on
the local drive to paths _within_ the specific container.

Using bind mounts allows you to create persistent storage for containers, and
pass configuration files into containers as well.

For example, the following command maps the `/host/data` dir on the host
to `/container/data` within the container, using the `ubuntu:latest` image
and launching the `/bin/bash` process:

```shell
docker run --rm -it -v /host/data:/container/data ubuntu:latest /bin/bash
```

### Lab Exercise

To put the basics together, use the following scenario:

- Use Docker to create an httpd server
  - Map host 8080 to 80
  - Mount a local directory for persistent storage of html assets
  - Do this with a `docker run` command.

::: details Solution

```shell
docker run --rm -p 8080:80 -d \
-v ~/Projects/docker/htdocs:/usr/local/apache2/htdocs \
httpd:latest
```

:::
