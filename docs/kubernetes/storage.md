---
title: Storage
---

## Storage In Kubernetes

In this section we'll cover the basics of Kubernetes storage.

### Managing Pod Volumes

Volumes allow you to store data in the pod using local storage. Here's an
example yaml file to create a container with volumes.

#### Sample Yaml File

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: vol
spec:
  containers:
    - name: centos
      image: centos:7
      command:
        - sleep
        - "3600"
      volumeMounts:
        - mountPath: /test
          name: test
  restartPolicy: Always
  volumes:
    - name: test
      emptyDir: {}
```

##### Breakdown

Let's break down what this file does. Based on what we already know,
`apiVersion`, `kind`, `metadata` are standard. The new stuff comes in the
`spec` section, starting from `volumeMounts`.

The base of the `spec` creates a container based on CentOS 7, which sleeps for
one hour, just so the container doesn't shut down.

The `volumeMounts` object shows us the path inside the container, as well as
the volume name specific to the cluster.

This is represented in the `volumes` tag of the container, where it's currently
set to `emptyDir:` which is a type. These types can be listed with:
`kubectl explain pods.spec.volumes`.

After the pod is created, you can use `kubectl exec -it` to the pod to test
writing files into the pod.

You can then use `kubectl describe` on the new pod to see the `mounts` and
`volume` info.

### Using Persistent Volumes

Persistent volumes decouple storage from pod development, so developers
can just depend on persistent volumes instead of developing from within the
pod.

`Persistent Volume` is the admin side that defines the volume.

`Persistent Volume Claim` is what the individual pods will use to claim
space on the PVs.

The PVC is an API object that uses the following spec:

```yaml
metadata:
  name: <claimName>
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Validate with `kubectl get pvc`

PV uses different specs:

```yaml
capacity:
accessMode:
storageClassName:
  - "optional"
persistentVolumeReclaimPolicy:
  - "what to do when a claim is deleted"
type:
  - "which storage type to use"
  - NFS
  - gcePersistentDisk
  - etc
```

Validate with `kubectl get pv`

### Set up Pods to Use Persistent Volumes

To allow pods to use the volume, you need to add the `volumes:` under `spec:`

```yaml
spec:
  volumes:
    - name: <internal storage name>
      persistentVolumeClaim:
        claimName: <claimName>
  containers: ...
```

Validate the pod bound to the volume with `kubectl get pvc`

:::warning
Don't bind to the local host in a prod env, ofc. Use NFS or something.
:::

### Dynamic Provisioning

This is where the `StorageClass` must be referred to in the PVC.

#### Provisioner

Key element, it's the interface that the storage type is read from

#### StorageClass

Parameters in the storage class contain provisioner specific information with
how to connect to the actual storage. You can set a `default` storageClass so
you don't have to define it.

### Using ConfigMaps

`ConfigMaps` separate configuration from code. They are clear text, where as
`Secrets` are base64 encoded. `Secrets` are mainly used for variables (keys,
etc)

#### Working with ConfigMaps

`ConfigMaps` create from file, which contains configuration info for the pod,
eg: nginx server configuration. Note `cm` as the resource type.

```shell
kubectl create cm <name> --from-file <filename>
```

To use in a pod, it's under `spec.volumes.name.configMap`:

```yaml
spec:
  containers:
    - name: nginx
      image: nginx
      volumeMounts:
        - name: conf
          mountPath: /container/path/
  volumes:
    - name: conf
      configMap:
        name: <cm-name>
        items:
          - key: <filename>
            path: <container file.conf>
```

### Using Secrets

Need `base64` encoding, use `kubectl create secret` and you can use
`--dry-run -o yaml > output.yaml` to use the output.

```shell
kubectl create secret generic mysecret \
--from-literal=username=user \
--from-literal=password=pass
```

`kubectl get secret mysecret -o yaml` should show user and pass both base64
encoded:

```yaml
apiVersion: v1
data:
  password: cGFzcw==
  username: dXNlcg==
kind: Secret
metadata:
  creationTimestamp: "2022-01-14T22:55:50Z"
  name: mysecret
  namespace: default
  resourceVersion: "19253"
  uid: 929ca037-ecc1-4c04-9e6c-52d9c9c4e1bc
type: Opaque
```

#### Secrets As Vars

You can also use secrets as vars (probably more popular) via the `env:`
For:

```shell
kubectl create secret mysql generic --from-literal=password=mysqlrootpwd
```

We can call the secret and the key under `spec:` using:

```yaml
spec:
  containers:
    - name: mysql
      image: mysql:latest
      env:
        - name: MYSQL_ROOT_PWD
          valueFrom:
            secretKeyRef:
              name: mysql
              key: password
```
