---
title: Managing Applications
---

## Managing Kubernetes Applications

### Deploying Applications

We'll primarily be working with `kubectl` here, since it's scaleable.

#### Application Architecture

Kubernetes application architecture shines some light on how these
microservices can support both high availability and scalability in a much
more efficient way than you can with VMs. The architecture contains
`resources` of which the major types are:

##### Pods

Kubernetes deploys `pods`, which contain a container (or multiple) and
a volume at a minimum.

##### Replica Sets

To take advantage of Kubernetes, really, the `pods` need to be managed in
a replicated way, so if one falls the replicas can take it's place.

To do this, Kubernetes uses the `replica set`, which simply takes care of the
replication.

##### Deployments

The `pods` belong to `replica sets` which belong to `deployments`, which,
at a high level, makes the `deployment` your application.

### Deployment Methods

There are many ways to create deployments in Kubernetes, from web based
dashboards, to CLI and it's many options. One of the major benefits of
Kubernetes is support for a full DevOps toolchain, meaning repeatability,
scalability, and versioning. Basically _imperative_ vs _declarative_.

#### Imperative via `kubectl`

As a reference, let's look at an imperative deployment using `kubectl`, which
simple creates a pod named `ingress-nginx` based off the `nginx` image:

```shell
kubectl create deployment ingress-nginx --image=nginx
```

#### Declarative via `yaml`

Large environments mean everyone should be doing the same thing, and by using
`yaml` files in a git repo, it's easy to both maintain consistency and
currency of your deployments.

:::tip
You can use JSON instead of yaml, but _reasons_ 🤷
:::

There are a few `kubectl` commands that make working with `yaml` easier.

##### explain

```shell
kubectl explain pod|deployments|etc
```

`explain` can be used to describe specific resources and get information

##### create and delete

```shell
kubectl create -f myapp.yml
kubectl delete -f myapp.yml
```

As an example, we can delete the previous deployment `ingress-nginx` with:

```shell
kubectl delete deployment ingress-nginx
```

##### get

`create` and `delete` can be used to do their respective commands based on
a `yaml` file, which can be generated with:

```shell
kubectl get <resources> -o yaml > output.yml
```

### Managing Pods

Kubernetes pods should not be started directly from the command line. You can
do so with the `kubectl run` command but that is or soon will be deprecated.

#### YAML Manifests

With `kubectl` you can supply the `-f` and a `yaml` file to create a pod:

```shell
kubectl create -f filename.yaml
```

The provided yaml file can be named anything logical, and requires the
following arguments at a minimum:

```yaml
apiVersion:
kind:
metadata:
spec:
```

#### Commands

Some basic commands for managing pods.
:::tip
A `pod` is a resource, so it can be replaced in commands with other
resources such as `deployment`
:::

```shell
kubectl get pods
```

Show running pods in current context and namespace. Use `-n namespace`
to target a specific namespace.

```shell
kubectl describe pods
```

Shows details about a running pod

```shell
kubectl get pods -o yaml > <filename>.yaml
```

Outputs the pod yaml configuration to an output file. This can be used to
make changes or create new pods based on the captured manifest file.

```shell
kubectl delete pods <pod>
```

Deletes a pod. If created by a yaml file, you should use the
`-f <filename>.yaml` like so:

```shell
kubectl delete pods -f <filename>.yaml
```
