---
title: Kubernetes Basics
---

## Introduction to Kubernetes Basics

::: tip
Kubernetes is shortened as k8s, where the 8 denotes the number of
characters between the `k` and the `s` in `kubernetes`.
:::

### K8S Architecture

#### Cluster

A collection of nodes that make up a cluster.

##### Control Nodes

Control Nodes run the vital services for your Kubernetes cluster
Multiple control nodes for HA

##### Worker Nodes

Control Node bots scale to run Kubelets on Worker Nodes

### Local K8S Installations

There are many options for local k8s development, such as MiniKube, Kind,
k3s, k0s, etc.

We'll have notes here for two in particular but pick whatever you feel
comfortable with or works for you.

#### Kind

Kind stands for **K**ubernetes **In D**ocker, which denotes my preferred
method, running in Docker containers. Perhaps not the best for a production
env, but for local dev and playing, being able to destroy and restart
containers makes things easy.

#### k3s

We'll look at k3s later, but the main reason for looking at this is
the lightweight architecture and support for ARMv7+ (looking at you,
rPi 4).

### Kubernetes Utilities

There are a few tools that are required and useful for managing your cluster.

#### kubectl

`kubectl` is the CLI for kubernetes. This allows access to cluster management
from the command line. I set this to the alias of `k`.

#### kubectx

`kubectx` is an optional utility that lists contexts and allows you to change
more efficiently than you can with `kubectl`. I set this to the alias `kc`.

#### kubens

`kubens` is basically `kubectx` for namespaces as opposed to clusters.
I set this to alias `kn`.

### Working with the API

Kubernetes uses a collection of APIs to intepret instructions in yaml files.
There are a few things to note when working with the API, namely, declaring
the version of the API with the `apiVersion` object at the top of the file.

#### api-resources

```shell
kubectl api-resources
```

Will show API resources in the command line.

#### api-versions

```shell
kubectl api-versions
```

Outputs the different API versions that are in use.

#### explain

```shell
kubectl explain pod
```

`explain` can be used to determine what the required fields are for a specific
object. Let's take a look at the `pod` example above:

::: details kubectl explain pod

```shell
KIND:     Pod
VERSION:  v1

DESCRIPTION:
     Pod is a collection of containers that can run on a host. This resource is
     created by clients and scheduled onto hosts.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     Standard object's metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Specification of the desired behavior of the pod. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

   status       <Object>
     Most recently observed status of the pod. This data may not be up to date.
     Populated by the system. Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status
```

:::
