---
title: Running Applications
---

## Running Kubernetes Applications

### Understanding Deployments

Deployments are the standard for running applications, and manages pods by
starting them from the deployment template specification.

Deployments enable scalability and availability via:

- Replication
  - How many instances of a pod do you want to be running?
  - ReplicaSet, deployment makes sure the number of pods are always available
- Upgrade Strategy
  - Upgrade without downtime if applications supports it

#### Labels

Labels serve many functions, including but not limited to:

- Labels are used by the API to connect objects
- ReplicaSets monitor the label to verify the amounts of pods are available

Labels are set automatically upon deployment but can be set manually with
`kubectl label`.

To get all labels:

```shell
kubectl get all --show-labels
```

### Running Applications in Deployments

This is the standard way of running applications. Not recommended to run pods
naked. A recap of the methods to deploy from CLI can be found here:

- [Imperative](./managing-applications.md#imperative-via-kubectl)
- [Declarative](./managing-applications.md#declarative-via-yaml)
- Just don't use `kubectl run`

#### Namespaces

Namespaces provide isolated environments for pods. For example, running two
Nginx pods in a single cluster, each in separate namespaces, means they
are isolated from each other, and names of resources must be unique within
namespaces.

Namespaces are useful in projects with multiple teams or projects within a
single cluster.

They can also be used to divide and manage cluster resources, eg split 8GB of
memory 6/2 across namespaces.

By default, each cluster contains a `default` and `kube-system` namespace.

#### Managing Namespaces

There are a few commands we can use to create namespaces, outside of `kubens`
that we covered earlier.

Create:

```shell
kubectl create ns <namespace>
```

List:

```shell
kubectl get all --all-namespaces
kubectl get all -A
```

You can also specify namespaces in commands with the `-n` flag.

#### Contexts

Contexts are the higher level of clustername and namespace that you're
connected to.

Context can be checked by:

```shell
kubectl config get
```

If you're managing multiple clusters, then the context will be relevant, and
each context can contain multiple namespaces.

Use `kubectx` utility to easily switch and list contexts.

### Scaling

Manage the scalability of your deployments through the number of replica sets.
ReplicaSet count can be modified through either the `edit` or `scale` commands
of `kubectl`.

Using this example imperative run:

```shell
kubectl create deployment test --image=nginx
```

We can see through `kubectl explain deployment` that the default value
of replicaSets is `1`.

#### kubectl edit

Using the sample deployment above, here's the output of `k edit deployment test`:
::: details k edit deployment output

```yaml
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2022-01-07T16:26:21Z"
  generation: 1
  labels:
    app: test
  name: test
  namespace: default
  resourceVersion: "396016"
  uid: 9660b208-f77f-42dc-95b9-c04c7ed42309
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: test
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: test
    spec:
      containers:
        - image: nginx
          imagePullPolicy: Always
          name: nginx
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status:
  availableReplicas: 1
  conditions:
    - lastTransitionTime: "2022-01-07T16:26:24Z"
      lastUpdateTime: "2022-01-07T16:26:24Z"
      message: Deployment has minimum availability.
      reason: MinimumReplicasAvailable
      status: "True"
      type: Available
    - lastTransitionTime: "2022-01-07T16:26:21Z"
      lastUpdateTime: "2022-01-07T16:26:24Z"
      message: ReplicaSet "test-5f6778868d" has successfully progressed.
      reason: NewReplicaSetAvailable
      status: "True"
      type: Progressing
  observedGeneration: 1
  readyReplicas: 1
  replicas: 1
  updatedReplicas: 1
```

:::

Notice under the `spec:` we can see the `replicas: 1`. This should already
be opened in whatever default editor is set in the shell. Change it to 3
and save.

`kubectl get deployments` now outputs 3 replicas.

#### kubectl scale

Using `edit` you can edit more than just the `replicas:` value, but if that's
all you're going after, we can use `scale` instead. Let's take the 3 replicas
we just created and take that back down to 1.

```shell
kubectl scale --replicas=1 deployment test
```

This scales the `replicas:` back to 1, for resource `deployment` named `test`.

Now if we do `kubectl get deployments` we'll see that the count returns to `1`.

:::tip
As a callback to managing applications, we can now delete the deployment with:
`kubectl delete deployment test` which gets rid of both the deployment AND
the pod.
:::

### Updates and Rollbacks

Kubernetes retains a deployment history for major updates to deployments.
When the history is changed, Kubernetes creates a new replica set and keeps
the old one with zero pods. This way, if a rollback is necessary it easy.

To prep for the example, redeploy the nginx container using an old version.

:::tip
If you want to create the deployment via yaml, you can generate a sample
yaml file using the `--dry-run=client --output=yaml` and `>` to an output file
as seen in the YAML block below. Just note that for a normal deployment, you
can omit the image version tag (`:1.8`) included in this example.
:::

:::: code-group
::: code-group-item CMD

```shell
kubectl create deployment rollingtest --image=nginx:1.8
```

:::
::: code-group-item YAML

```shell
kubectl create deployment nginx --image=nginx:1.8 --dry-run=client \
--output=yaml > nginx.yaml

kubectl create -f nginx.yaml
```

:::
::::

#### kubectl rollout

Let's take a look at a simple example of a rollout.

##### history

When we run `kubectl rollout history deployment` we can see that our
`rollingtest` deployment is at revision 1.

Let's run `kubectl edit deployment rollingtest` on the deploment to change
the nginx version. In the output under `spec:` we can see the
`- image: nginx:1.8`, which we can upgrade to `1.15`.

Now running `kubectl rollout history deployment rollingtest` we can see
revisions 1 and 2 in the list.

If we run a `kubectl get all` we can also see that there's a new replica set
for our `rollingtest` deployment with 0 pods.

##### --revision

You can use the `--revision=<version>` flag on the command to see the diffs:

```shell
kubectl rollout history deployment rollingtest --revision=1
kubectl rollout history deployment rollingtest --revision=2
```

zero downtime updates example use nginx:1.8, then edit and change to 1.15

##### undo

Let's clean up what we just did with `undo` and take 2 back to 1.

```shell
kubectl rollout undo deployment rollingtest --to-revision=1
```

:::tip
Clean up again with `kubectl delete deployment rollingtest`
:::

#### Strategy

Strategy is listed on the `deployment.spec.strategy` object and has two options.

##### Recreate

Recreate will shutdown all versions and run new ones. Requires downtime.

##### RollingUpdate

If an application supports it, RollingUpdate will update each replica one
by one, for zero downtime upgrades.
